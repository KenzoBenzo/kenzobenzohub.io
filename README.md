## kenzobenzo.github.io
This is my personal portfolio & first attempt at understanding github!

# A few links of other places you can find me:
1. Twitter https://twitter.com/KenzSmutz
2. Medium https://medium.com/@makennasmutz
3. LinkedIn https://www.linkedin.com/in/makennasmutz/


If you have any questions, are an ambitous entrepreneur or just want to make friends, makennasmutz@gmail.com is my email or Tweet me!
